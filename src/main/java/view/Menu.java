package view;

public enum Menu {
    SHOW_DETERGENTS(1, "Enter 1 if you want to see all detergents"),
    BUY_DETERGENTS(2, "Enter 2 if you want to buy detergents"),
    EXIT(3, "Enter 3 to exit");
    private final Integer index;
    private final String message;

    Menu(Integer index, String message) {
        this.index = index;
        this.message = message;
    }

    public Integer getIndex() {
        return index;
    }

    public String getMessage() {
        return message;
    }

    public static void showMenu() {
        System.out.println("Please, make your choice:");
        for (Menu menu : Menu.values()
        ) {
            System.out.println(menu.getIndex() + ":" + menu.getMessage());
        }
    }

    public static Menu getIndexOfChoice(Integer index) {
        for (Menu menu : Menu.values()
        ) {
            if (menu.getIndex().equals(index)) {
                return menu;
            }
        }
        return null;
    }
}

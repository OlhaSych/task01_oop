package view;

import controller.Controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class View {
    private static final BufferedReader READER = new BufferedReader(new InputStreamReader(System.in));

    public static void executeStatement() throws IOException {
        while (true) {
            Menu.showMenu();
            Integer index = Integer.valueOf(READER.readLine());
            Menu menu = Menu.getIndexOfChoice(index);
            if (menu == null) {
                System.out.println("There is no such index!");
                return;
            }
            switch (menu) {
                case SHOW_DETERGENTS:
                    Controller.TRADER.showDetergents();
                    break;
                case BUY_DETERGENTS:
                    Controller.buyDetergents();
                    break;
                case EXIT:
                    System.out.println("Bye!");
                    return;
                default:
                    System.out.println("There is no such option!");
                    break;
            }
        }
    }
}

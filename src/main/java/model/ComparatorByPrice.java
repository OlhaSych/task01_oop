package model;

import java.util.Comparator;

public class ComparatorByPrice implements Comparator<Detergent> {
    public int compare(Detergent o1, Detergent o2) {
        return o1.getPrice().compareTo(o2.getPrice());
    }
}

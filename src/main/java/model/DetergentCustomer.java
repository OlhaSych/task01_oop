package model;

public interface DetergentCustomer {
    boolean buy(int index);
}

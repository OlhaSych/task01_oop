package model;

import controller.Controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Buyer implements DetergentCustomer {
    private String name;
    private final List<Detergent> detergents = new ArrayList<Detergent>();
    private Integer totalPrice;

    public Buyer(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Detergent> getDetergents() {
        return detergents;
    }

    public Integer getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Integer totalPrice) {
        this.totalPrice = totalPrice;
    }

    @Override
    public String toString() {
        return "Buyer{" +
                "name='" + name + '\'' +
                '}';
    }

    public boolean buy(int index) {
        boolean buyDetergent = false;
        if (index >= 0 && index < Controller.TRADER.getDetergents().size()) {
            Detergent detergent = Controller.TRADER.getDetergents().get(index);
            if (detergents.add(detergent)) {
                System.out.println("detergent was bought");
                buyDetergent = true;
            } else {
                System.out.println("There is no such option!");
            }
        }
        return buyDetergent;
    }

    public void showUserDetergents() {
        if (detergents.isEmpty()) {
            System.out.println("There is no detergents in the list");
        } else {
            Collections.sort(detergents, new ComparatorByPrice());
            System.out.println("You have already bought: ");
            totalPrice = 0;
            for (int i = 0; i < detergents.size(); i++) {
                System.out.println(i + " : " + detergents.get(i));
                totalPrice = totalPrice + detergents.get(i).getPrice();
            }
            System.out.println("Total price of your order is :" + totalPrice);
        }
    }
}

package model;

public abstract class Detergent {
    private String brand;
    private Integer price;

    public Detergent(String brand, Integer price) {
        this.brand = brand;
        this.price = price;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Detergent{" +
                "brand='" + brand + '\'' +
                ", price=" + price +
                '}';
    }
}

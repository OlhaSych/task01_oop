package model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DetergentTrader {
    private String name;
    private final List<Detergent> detergents = new ArrayList<Detergent>();

    public DetergentTrader(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Detergent> getDetergents() {
        return detergents;
    }

    public void showDetergents() {
        if (detergents.isEmpty()) {
            System.out.println("There is no elements in the list");
        } else {
            Collections.sort(detergents, new ComparatorByPrice());
            for (int i = 0; i < detergents.size(); i++) {
                System.out.println(i + " : " + detergents.get(i));
            }
        }
    }

    public boolean addDetergents(Detergent detergent) {
        if (detergents.add(detergent)) {
            return true;
        }
        return false;
    }

}

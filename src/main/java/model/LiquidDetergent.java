package model;

public class LiquidDetergent extends Detergent {
    private Double volume;
    private boolean flavored;

    public LiquidDetergent(String brand, Integer price, Double volume, boolean flavored) {
        super(brand, price);
        this.volume = volume;
        this.flavored = flavored;
    }

    public Double getVolume() {
        return volume;
    }

    public void setVolume(Double volume) {
        this.volume = volume;
    }

    public boolean isFlavored() {
        return flavored;
    }

    public void setFlavored(boolean flavored) {
        this.flavored = flavored;
    }

    @Override
    public String toString() {
        return "Liquid Detergent{brand: " +
                super.getBrand() + ", price :" +
                super.getPrice() + "," +
                "volume =" + volume +
                ", flavored= " + flavored +
                '}';
    }
}

package model;

public class PowderDetergent extends Detergent {
    private Integer weight;
    private boolean bleach;

    public PowderDetergent(String brand, Integer price, Integer weight, boolean bleach) {
        super(brand, price);
        this.weight = weight;
        this.bleach = bleach;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public boolean isBleach() {
        return bleach;
    }

    public void setBleach(boolean bleach) {
        this.bleach = bleach;
    }

    @Override
    public String toString() {
        return "Powder Detergent{brand : " +
                super.getBrand() + ", price: " +
                super.getPrice() + ", " +
                "weight=" + weight +
                ", bleach=" + bleach +
                '}';
    }
}

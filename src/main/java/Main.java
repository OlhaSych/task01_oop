import controller.Controller;
import model.Detergent;
import model.LiquidDetergent;
import model.PowderDetergent;
import view.View;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {
        Detergent detergent1 = new PowderDetergent("ARIEL", 150, 850, true);
        Detergent detergent2 = new PowderDetergent("TIDE", 130, 900, false);
        Detergent detergent3 = new LiquidDetergent("Fairy", 34, 1000.0, false);
        Detergent detergent4 = new LiquidDetergent("Gala", 26, 1000.0, true);
        Controller.TRADER.getDetergents().add(detergent1);
        Controller.TRADER.getDetergents().add(detergent2);
        Controller.TRADER.getDetergents().add(detergent3);
        Controller.TRADER.getDetergents().add(detergent4);
        View.executeStatement();
    }
}

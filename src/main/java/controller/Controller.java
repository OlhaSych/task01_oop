package controller;

import model.Buyer;
import model.DetergentTrader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Controller {
    private static final BufferedReader READER = new BufferedReader(new InputStreamReader(System.in));
    private static final Buyer BUYER = new Buyer("Consumer");
    public static final DetergentTrader TRADER = new DetergentTrader("EVA");

    public static void buyDetergents() throws IOException {
        while (true) {
            System.out.println("Enter index of detergent you want to buy:");
            TRADER.showDetergents();
            int index = Integer.valueOf(READER.readLine());
            BUYER.buy(index);
            System.out.println("Do you want to buy more detergents?");
            System.out.println("Enter 1 - yes");
            System.out.println("Enter 2 - no");
            int indexOfOtherDetergents = Integer.valueOf(READER.readLine());
            switch (indexOfOtherDetergents) {
                case 1:
                    break;
                case 2:
                    BUYER.showUserDetergents();
                    return;
                default:
                    System.out.println("There is no such option!");
            }
        }
    }
}
